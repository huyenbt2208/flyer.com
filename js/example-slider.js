$(document).ready(function(){
    $('.slick-player').owlCarousel({
        items:1,
        center:true,
        nav:false,
        autoplay: true,
        autoplayTimeout: 3000,
        autoHeight: true
    });

});
$(document).ready(function () {
  // Menu ---------------------------//
  $('.menuopen').on('click', function() {
    if($('body').hasClass('fix_active')) {
      $('body').removeClass('fix_active open_menu');
      $('.fixmenu').fadeOut();
      $('.overlay').fadeOut();
      $('#contents').css('padding-top','');
       $('.fixmenu').css('top',$('#header').height());
    } else {
      $('.fixmenu').fadeIn();
      $('.overlay').fadeIn();
      $('body').addClass('fix_active open_menu');
      $('#contents').css('padding-top',$('#header').height());
      $('.fixmenu').css('top',$('#header').height());
    }
  });
    
});